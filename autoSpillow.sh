#!/bin/bash

#This script was made by Arthur Amalvy


source=""
targetName=""

#Un script pour les gouverner tous, un script pour les trouver.. Un script pour les amener tous, et dans les tenêbres les LIER !
find -name "*.c" -print > autoSpillowToken.sp


while read line 
do 
	echo -e "$line\n"
	source=`echo -e "$line\n"`

	targetName=`basename "$source"`
	targetName="${targetName/.c/.html}"

	./Spillow.sh "$source" "documentation/algorithmes/$targetName"
	
done < autoSpillowToken.sp

rm -f autoSpillowToken.sp

echo -e "autoSpillow : done.\n"
