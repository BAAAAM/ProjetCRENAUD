# Projet : CRENAUD

Le Projet CRENAUD ( Catch Royal Edition Nazi : An Ultimate Dictator ) est un jeu humoristique de gestion de compétition de catch en interface non-graphique. Pourrez vous devenir le plus grand manager de catch du monde ?


# Installation 

Dans le dossier _build_ :

```sh
cmake ..
make
```

L'éxécutable */buuid/bin/Programme* est généré.


# Lancement

**Attention : le jeu doit impérativement être lancé depuis la racine du projet sous peine d'une errer de segmentation**
