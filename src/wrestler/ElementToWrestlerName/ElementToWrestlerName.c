/*
* @Author: klmp200
* @Date:   2016-05-25 00:47:41
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-25 00:48:16
*/

#include <stdio.h>
#include <stdlib.h>

#include "../../lists/list.h"
#include "../../wrestler/wrestler.h"

char * ElementToWrestlerName(S_element *element){

	return ((S_wrestler*)element->data)->name;

}
