#ifndef ELEMENTTOWRESTLERNAME_H_INCLUDED
#define ELEMENTTOWRESTLERNAME_H_INCLUDED

#include "../../lists/list.h"

char * ElementToWrestlerName(S_element *);

/*
	Cast an element into a wrestler name
*/

#endif // ELEMENTTOWRESTLERNAME_H_INCLUDED
