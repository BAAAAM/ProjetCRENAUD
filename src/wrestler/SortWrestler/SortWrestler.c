#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "../S_wrestler.h"
#include "../../lists/list.h"
#include "../wrestler.h"


//Use the gnome sorting algorithm
int SortWrestler(S_list *wList){ 


	S_list *temp = NULL;
	S_element *temporaryElement = temp->first;

	S_element *element = NULL;
	element = wList->first;
	element = element->next;

	int ok = 1;
	
	if (wList != NULL){

		ok = 0;

		while(element != NULL){

			if (toupper(ElementToWrestlerName(element->previous)[0]) <= toupper(ElementToWrestlerName(element)[0])){

				element = element->next;

			}

			else{
				
				temporaryElement = temp->first;

				*temporaryElement = *element;
				//element = temp->first;
				//strcpy(temp, element);
				element = element->previous;
				//strcpy(element, element->previous);
				*element->previous = *temporaryElement;
				temporaryElement = NULL;
				//strcpy(element->previous, temp);
				free(temp);


			}
			
		}

	}
	return ok;
}
