#include <stdio.h>
#include <stdlib.h>

#include "../../lists/list.h"
#include "../S_wrestler.h"
#include "../../player/player.h"
#include "../../status/status.h"

void TimeSWrestler(S_list *list, S_player *player){

	int i=0;
	S_status *status = NULL;
	S_element *element = NULL;

	element = list->first;

	while(element != NULL){



		for(i=0;i<MAX_W_STATUS;i++)
		{

			status = ((S_wrestler*)element->data)->status[i];

			if (status != NULL){

				player->money = player->money - status->mMoney;
				((S_wrestler*)element->data)->status[i] = CheckStatus(status);

			}

		}

		element = element->next;
	}

}
