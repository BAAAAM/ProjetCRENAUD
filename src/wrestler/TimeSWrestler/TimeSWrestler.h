#ifndef TIMESWRESTLER_H_INCLUDED
#define TIMESWRESTLER_H_INCLUDED

#include "../../lists/list.h"
#include "../S_wrestler.h"
#include "../../player/player.h"
#include "../../status/status.h"

void TimeSWrestler(S_list *, S_player *);

#endif // TIMESWRESTLER_H_INCLUDED