#ifndef CONCATWRESTLERLIST_H_INCLUDED
#define CONCATWRESTLERLIST_H_INCLUDED

S_list * ConcatWrestlerList(S_list *, S_list *);

/*
	Concatenate two lists, by adding them and supressing redundant entries. Not tested yet.
*/

#endif //CONCATWRESTLERLIST_H_INCLUDED
