/*
* @Author: klmp200
* @Date:   2016-05-30 15:44:51
* @Last Modified by:   klmp200
* @Last Modified time: 2016-06-02 21:36:48
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "../../lists/list.h"
#include "../S_wrestler.h"
#include "../ElementToWrestlerName/ElementToWrestlerName.h"
#include "../FreeWrestler/FreeWrestler.h"


S_list * ConcatWrestlerList(S_list *list1, S_list *list2){

	S_element *element;
	char *toTest = NULL;

	if (list1 != NULL && list2 != NULL){

		while (list2->first != NULL){

			toTest = ElementToWrestlerName(list2->first);

			element = list1->first;

			while (element != NULL){

				if (strcmp(toTest, ElementToWrestlerName(element)) == 0){

					FreeWrestler((S_wrestler*)element->data);
					PopPtnList(list1, element);
					element = NULL;

				} else {

					element = element->next;

				}

			}

			AppendList(list1, list2->first->data, sizeof(S_wrestler));
			PopPtnList(list2, list2->first);

		}

		FreeList(list2);

	}

	return list1;
}
