#include <stdio.h>
#include <stdlib.h>


#include "../S_wrestler.h"
#include "../../player/player.h"
#include "../../status/status.h"

int FameC(S_wrestler A,S_wrestler B,S_player * P){ //wrestler A, B and player

	int C=0, D=0, E=0, F=0, G=0;
	int FA=A.fame;
	int FB=B.fame;
	int SA=A.strength;
	int SB=B.strength;
	int i=0;

	for(i=0;i<MAX_W_STATUS;i++){

		if(A.status[i] != NULL){

			P->fame=P->fame-A.status[i]->mMoney;
			FA=FA+A.status[i]->bFame;
			SA=SA+A.status[i]->bStrength;

		}
	}

	for(i=0;i<MAX_W_STATUS;i++){

		if(B.status[i]!=NULL){

			P->fame=P->fame-B.status[i]->mMoney;
			FB=FB+B.status[i]->bFame;
			SB=SB+B.status[i]->bStrength;

		}
	}

	F=abs(FA);

	G=abs(FB);

	D=abs(abs(SA)-abs(SB));

	E=abs(F-G);

	if (D==0){

		D=1;

	}

	if (E==0){

		E=1;

	}

	C = 1000 + ((100 / E) * (F + G)) / D;

	if (FA<0){

		if (FB<0){

			C = C / 2;

		}

	}

	if (FA>0){

		if (FB>0){

			C=C/2;

		}

	}

	C=C+(F/2)*10;


	P->fame=P->fame+C;
	P->money=P->money+C/10;

	return C;
}