#ifndef FAMEC_H_INCLUDED
#define FAMEC_H_INCLUDED

#include "../S_wrestler.h"
#include "../../player/player.h"
#include "../../status/status.h"

int FameC(S_wrestler,S_wrestler,S_player *); //wrestler A, B and player

#endif // FAMEC_H_INCLUDED