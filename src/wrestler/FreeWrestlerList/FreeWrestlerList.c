/*
* @Author: klmp200
* @Date:   2016-05-16 17:42:53
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-16 17:52:48
*/

#include <stdio.h>
#include <stdlib.h>

#include "../../lists/list.h"
#include "../S_wrestler.h"
#include "../FreeWrestler/FreeWrestler.h"

int FreeWrestlerList(S_list *list){
	int ok = 1;

	if (list != NULL){
		ok = 0;
		while (list->first != NULL && ok != 1){
			ok = FreeWrestler((S_wrestler*)list->first->data);
			PopPtnList(list, list->first);
		}
		FreeList(list);
	}

	return ok;
}