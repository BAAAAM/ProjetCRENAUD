#ifndef FREEWRESTLERLIST_H_INCLUDED
#define FREEWRESTLERLIST_H_INCLUDED

#include "../../lists/list.h"

int FreeWrestlerList(S_list *);

/*
	Free a list of wrestlers
	Return 0 if success and 1 if failure
*/

#endif // FREEWRESTLERLIST_H_INCLUDED