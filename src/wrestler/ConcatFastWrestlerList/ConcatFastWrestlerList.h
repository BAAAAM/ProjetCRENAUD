#ifndef CONCATFASTWRESTLERLIST_H_INCLUDED
#define CONCATFASTWRESTLERLIST_H_INCLUDED

#include "../../lists/list.h"

S_list * ConcatFastWrestlerList(S_list *, S_list *);

/*
	Put content of the second list in the first one
	Free the second list
*/

#endif // CONCATFASTWRESTLERLIST_H_INCLUDED