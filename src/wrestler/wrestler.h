#ifndef WRESTLER_H_INCLUDED
#define WRESTLER_H_INCLUDED

#include "S_wrestler.h"
#include "InitiateWrestler/InitiateWrestler.h"
#include "FreeWrestler/FreeWrestler.h"
#include "FreeWrestlerList/FreeWrestlerList.h"
#include "SortWrestler/SortWrestler.h"
#include "FameC/FameC.h"
#include "ElementToWrestlerName/ElementToWrestlerName.h"
#include "ConcatWrestlerList/ConcatWrestlerList.h"
#include "ConcatFastWrestlerList/ConcatFastWrestlerList.h"
#include "TimeSWrestler/TimeSWrestler.h"
#include "ApplyRandomEffect/ApplyRandomEffect.h"

#endif // WRESTLER_H_INCLUDED
