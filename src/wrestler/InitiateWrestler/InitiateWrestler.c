/*
* @Author: klmp200
* @Date:   2016-05-13 15:54:07
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-21 18:53:16
*/

#include <stdio.h>
#include <stdlib.h>

#include "../S_wrestler.h"

S_wrestler InitiateWrestler(){
	S_wrestler wrestler;
	int i;

	wrestler.name = NULL;
	wrestler.description = NULL;
	wrestler.ascii = NULL;

	wrestler.price = 0;
	wrestler.fame = 0;
	wrestler.strength = 0;

	for (i=0;i<MAX_W_STATUS;i++){

		wrestler.status[i] = NULL;

	}

	return wrestler;
}