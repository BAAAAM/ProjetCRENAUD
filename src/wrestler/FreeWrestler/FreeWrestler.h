#ifndef FREEWRESTLER_H_INCLUDED
#define FREEWRESTLER_H_INCLUDED

#include "../S_wrestler.h"

int FreeWrestler(S_wrestler *);

/*
	Free content of a wrestler
	Return 0 if success and 1 if failure
*/

#endif // FREEWRESTLER_H_INCLUDED