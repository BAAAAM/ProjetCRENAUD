#ifndef POP_H_INCLUDED
#define POP_H_INCLUDED

#include "../S_list.h"

int PopList(S_list *, int);

/*
	Delete a selected element from a list
	Return 0 if success and 1 if failure
*/

#endif // POP_H_INCLUDED