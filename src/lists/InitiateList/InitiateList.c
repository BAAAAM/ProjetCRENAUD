/*
* @Author: klmp200
* @Date:   2016-04-29 18:52:51
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-02 18:43:23
*/

#include <stdio.h>
#include <stdlib.h>

#include "../S_list.h"
#include "../S_element.h"

S_list *InitiateList(){
	S_list *list = malloc(sizeof(*list));

	if(list != NULL){
		list->first = NULL;
		list->final = NULL;
		list->size = 0;
	}

	return list;
}