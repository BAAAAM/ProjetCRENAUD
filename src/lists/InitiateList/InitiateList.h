#ifndef INITIATELIST_H_INCLUDED
#define INITIATELIST_H_INCLUDED

#include "../S_list.h"
#include "../S_element.h"

S_list *InitiateList();

/*
	Allow to create a new list
	Return NULL in case of failure
*/

#endif // INITIATELIST_H_INCLUDED