/*
* @Author: klmp200
* @Date:   2016-04-29 18:59:32
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-02 22:37:10
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../S_element.h"
#include "../S_list.h"

int InsertList(S_list *list, void *data, int size){

	int ok = 0;

	void * newData = malloc(size);

	/* Create a new element */

	S_element *newElement = malloc(sizeof(*newElement));
	if(list != NULL && newElement != NULL && newData != NULL){
		memcpy(newData, data, size);
		newElement->data = newData;

		/* Insert the element into the list and update list */

		newElement->previous = NULL;

		if (list->first != NULL){
			list->first->previous = newElement;
		} else {
			list->final = newElement;
		}
		newElement->next = list->first;

		list->first = newElement;
		list->size = list->size + 1;

	} else {
		if (newElement != NULL){
			free(newElement);
		}
		if (newData != NULL){
			free(newData);
		}
		ok = 1;
	}

	return ok;
}