/*
* @Author: klmp200
* @Date:   2016-04-30 23:46:46
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-03 13:56:21
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../S_list.h"
#include "../S_element.h"

int AppendList(S_list *list, void *data, int size){
	int ok = 0;

	void *newData = malloc(size);

	S_element *newElement = malloc(sizeof(*newElement));
	if(list != NULL && newElement != NULL && newData != NULL){
		memcpy(newData, data, size);
		newElement->data = newData;
		newElement->next = NULL;
		if (list->final == NULL){
			list->first = newElement;
			list->final = newElement;
			newElement->previous = NULL;
		} else {
			newElement->previous = list->final;
			list->final->next = newElement;
			list->final = newElement;
		}

		list->size = list->size + 1;

	} else {
		if (newElement != NULL){
			free(newElement);
		}
		if (newData != NULL){
			free(newData);
		}
		ok = 1;
	}

	return ok;
}