#ifndef APPENDLIST_H_INCLUDED
#define APPENDLIST_H_INCLUDED

#include "../S_list.h"

int AppendList(S_list*, void*, int);

/*
	Insert void* at the end of a given list
	You should specify the size of the data in memory
	Return 0 if success and 1 if failure
*/

#endif // APPENDLIST_H_INCLUDED
