/*
* @Author: klmp200
* @Date:   2016-05-21 11:44:30
* @Last Modified by:   klmp200
* @Last Modified time: 2016-06-01 15:46:02
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "../S_list.h"
#include "../S_element.h"
#include "../GetElementList/GetElementList.h"

S_element * RndList(S_list *list){
	S_element *element = NULL;
	static int generator = 0;
	int rnd;

	if (!generator){
		srand(time(NULL));
		generator = 1;
	}

	if (list != NULL){

		rnd = rand() % list->size;
		element = GetElementList(list, rnd);

	}

	return element;
}
