#ifndef RNDLIST_H_INCLUDED
#define RNDLIST_H_INCLUDED

#include "../S_list.h"
#include "../S_element.h"

S_element * RndList(S_list *);

/*
	Return a random element in a given list
	Return NULL in case of error
*/

#endif // RNDLIST_H_INCLUDED