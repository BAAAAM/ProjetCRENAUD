#ifndef POPPTNLIST_H_INCLUDED
#define POPPTNLIST_H_INCLUDED

#include "../S_list.h"
#include "../S_element.h"

int PopPtnList(S_list *, S_element *);

/*
	Delete an element in a list
	It's safer to use PopList
	Return 0 if success and 1 otherwise
*/

#endif // POPPTNLIST_H_INCLUDED