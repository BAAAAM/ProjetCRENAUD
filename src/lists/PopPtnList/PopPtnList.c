/*
* @Author: klmp200
* @Date:   2016-05-06 22:58:11
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-06 23:03:42
*/

#include <stdio.h>
#include <stdlib.h>

#include "../S_list.h"
#include "../S_element.h"

int PopPtnList(S_list *list, S_element *element){
	int ok = 0;

	if (list != NULL && element != NULL){

		if (list->first == element&& list->final == element){
			list->first = NULL;
			list->final = NULL;

		} else if (list->first == element){
			list->first = element->next;
			element->previous = NULL;

		} else if (list->final == element){
			list->final = element->previous;
			element->previous->next = NULL; 

		} else {
			element->next->previous = element->previous;
			element->previous->next = element->next;
		}

		if (element->data != NULL){
			free(element->data);
		}
		free(element);

		list->size = list->size - 1;

	} else {
		ok = 1;
	}

	return ok;
}