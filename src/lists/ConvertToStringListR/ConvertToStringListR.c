/*
* @Author: klmp200
* @Date:   2016-05-18 22:14:11
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-18 22:18:24
*/

#include <stdio.h>
#include <stdlib.h>

#include "../S_list.h"
#include "../S_element.h"

#include "../PopPtnList/PopPtnList.h"

char * ConvertToStringListR(S_list *list, int drop){
	S_element *element = NULL;

	char *string = NULL;
	int i = 0;

	if (list != NULL){

		string = malloc(sizeof(char) * (list->size + 1));

	}

	if (string != NULL){

		element = list->final;

		while (element != NULL){

			string[i] = *((char*)element->data);
			element = element->next;

			if (drop){

				PopPtnList(list, list->final);

			}

			i++;
		}

		string[i] = '\0';
	}

	return string;
}