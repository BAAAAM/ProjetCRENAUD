/*
* @Author: klmp200
* @Date:   2016-04-30 19:24:00
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-03 12:52:14
*/

#include <stdio.h>
#include <stdlib.h>

#include "../S_list.h"
#include "../S_element.h"
#include "../GetElementList/GetElementList.h"

void * GetValueList(S_list *list, int nb){

	S_element *element = NULL;
	void * returnValue = NULL;

	if(list != NULL && nb < list->size){
		element = GetElementList(list, nb);
		if (element != NULL){
			returnValue = element->data;
		}
	}

	return returnValue;

}