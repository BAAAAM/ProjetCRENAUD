#ifndef GETVALUELIST_H_INCLUDED
#define GETVALUELIST_H_INCLUDED

#include "../S_list.h"

void * GetValueList(S_list *, int);

/*
	Get data from a specified position in a list
	Data is given into a void pointer
	Return NULL if failure
*/


#endif // GETVALUELIST_H_INCLUDED