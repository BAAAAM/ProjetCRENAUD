#ifndef DROPLIST_H_INCLUDED
#define DROPLIST_H_INCLUDED

#include "../S_list.h"

int DropList(S_list *);

/*
	Drop all data from a list
	Return 0 if Success and 1 if Failure
*/

#endif // DROPLIST_H_INCLUDED