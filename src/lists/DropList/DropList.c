/*
* @Author: klmp200
* @Date:   2016-04-30 14:45:33
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-03 15:56:37
*/

#include <stdio.h>
#include <stdlib.h>

#include "../S_list.h"
#include "../S_element.h"

int DropList(S_list *list){
	int ok = 0;
	S_element *current = NULL;

	if (list != NULL){
		current = list->first;
		while(current != NULL){
			S_element *toDelete = current;
			current = current->next;
			
			if (toDelete->data != NULL){
				free(toDelete->data);
			}

			free(toDelete);
		}
		list->first = NULL;
		list->size = 0;
	} else {
		ok = 1;
	}

	return ok;
}