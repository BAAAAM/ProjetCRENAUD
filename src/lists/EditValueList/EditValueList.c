/*
* @Author: klmp200
* @Date:   2016-04-30 18:10:12
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-03 21:41:12
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../S_list.h"
#include "../S_element.h"
#include "../GetElementList/GetElementList.h"

int EditValueList(S_list *list, void *data, int size, int nb){
	int ok = 0;
	void *newData = NULL;
	S_element *element = NULL;


	if (list != NULL && nb < list->size){
		newData = malloc(size);

		if(newData != NULL){
			element = GetElementList(list, nb);
			memcpy(newData, data, size);

			if(element->data != NULL){
				free(element->data);
			}

			element->data = newData;

		} else {
			ok = 1;
		}

	} else {
		ok = 1;
	}

	return ok;
}