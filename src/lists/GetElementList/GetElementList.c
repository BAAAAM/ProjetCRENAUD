/*
* @Author: klmp200
* @Date:   2016-04-29 19:41:40
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-02 18:45:11
*/

#include <stdio.h>
#include <stdlib.h>

#include "../S_element.h"
#include "../S_list.h"

S_element * GetElementList(S_list *list, int nb){
	S_element *current = NULL;
	int i;

	if (nb < list->size){

		if (nb == list->size - 1){
			current = list->final;

		} else if (nb == 0){
			current = list->first;

		} else if (nb <= (list->size - 1)/2){
			i = 0;
			current = list->first;
			while(i < nb){
				current = current->next;
				i++;
			}
		} else {
			i = list->size - 1;
			current = list->final;
			while(i > nb){
				current = current->previous;
				i = i - 1;
			}
		}

	}

	return current;
}