/*
* @Author: klmp200
* @Date:   2016-05-30 08:57:18
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-30 09:02:14
*/

#include <stdio.h>
#include <stdlib.h>

#include "../S_game.h"
#include "../../lists/list.h"
#include "../../player/player.h"
#include "../../status/status.h"
#include "../../wrestler/wrestler.h"

void FreeGame(S_game *game){

	if (game->wList != NULL){

		FreeWrestlerList(game->wList);
		game->wList = NULL;

	}

	if (game->sList != NULL){

		FreeStatusList(game->sList);
		game->sList = NULL;

	}


}