/*
* @Author: klmp200
* @Date:   2016-05-30 08:51:31
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-30 08:53:59
*/

#include <stdio.h>
#include <stdlib.h>

#include "../S_game.h"
#include "../../player/player.h"

S_game InitiateGame(){
	S_game game;

	game.wList = NULL;
	game.sList = NULL;
	game.player = InitiatePlayer("", 1000, 0);

	game.mode = 0;
	game.save = 0;
	game.victory = 0;

	return game;

}