/*
* @Author: klmp200
* @Date:   2016-05-11 15:38:26
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-11 21:42:57
*/

#include <stdio.h>
#include <stdlib.h>

#include "../S_gfile.h"
#include "../S_gyaml.h"

S_gfile * InitiateGfile(){
	S_gfile *gfile = malloc(sizeof(*gfile));	

	gfile->data = NULL;
	gfile->size = 0;

	return gfile;
}