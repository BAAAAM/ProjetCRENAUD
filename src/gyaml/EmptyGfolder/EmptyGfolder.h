#ifndef EMPTYGFOLDER_H_INCLUDED
#define EMPTYGFOLDER_H_INCLUDED

int EmptyGfolder(char[]);

/*
	Delete all .godwin files in a given folder
	Return 0 in case of success and 1 in case of failure
*/

#endif // EMPTYGFOLDER_H_INCLUDED