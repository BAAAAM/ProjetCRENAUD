/*
* @Author: klmp200
* @Date:   2016-05-27 10:45:32
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-27 11:05:58
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dirent.h>

#ifndef _WIN32
	#include <sys/types.h>
#endif

#include "../CheckExtension/CheckExtension.h"

int EmptyGfolder(char path[]){

	int ok = 1;	
	DIR *rep = NULL;
	struct dirent *file = NULL;

	int pathSize = strlen(path);
	int fileSize;
	char *fullPath = NULL;

	rep = opendir(path);

	if (rep != NULL){

		file = readdir(rep);

		while (file != NULL){

			fileSize = strlen(file->d_name);
			
			fullPath = malloc(sizeof(char) * (fileSize + pathSize + 1));

			if (fullPath != NULL){

				if(CheckExtension(file->d_name, strlen(file->d_name), "godwin", 6)){

					strcpy(fullPath, path);
					strcat(fullPath, file->d_name);

					remove(fullPath);

				}

				free(fullPath);

			}

			file = readdir(rep);

		}

		closedir(rep);
		ok = 0;

	}


	return ok;

}