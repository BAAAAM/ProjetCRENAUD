#ifndef GYAML_H_INCLUDED
#define GYAML_H_INCLUDED

#include "S_gyaml.h"
#include "S_gfile.h"
#include "FreeGyaml/FreeGyaml.h"
#include "ParseLineGyaml/ParseLineGyaml.h"
#include "InitiateGyaml/InitiateGyaml.h"
#include "LineToData/LineToData.h"
#include "LineToKey/LineToKey.h"
#include "ParseFile/ParseFile.h"
#include "InitiateGfile/InitiateGfile.h"
#include "SetSizeGfile/SetSizeGfile.h"
#include "FreeGfile/FreeGfile.h"
#include "LoadGfolder/LoadGfolder.h"
#include "CheckExtension/CheckExtension.h"
#include "WriteGfile/WriteGfile.h"
#include "WriteGyamlKey/WriteGyamlKey.h"
#include "WriteGyamlData/WriteGyamlData.h"
#include "EmptyGfolder/EmptyGfolder.h"

#endif // GYAML_H_INCLUDED