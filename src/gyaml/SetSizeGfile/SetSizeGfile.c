/*
* @Author: klmp200
* @Date:   2016-05-11 15:44:27
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-12 13:50:00
*/

#include <stdio.h>
#include <stdlib.h>

#include "../S_gyaml.h"
#include "../S_gfile.h"

int SetSizeGfile(S_gfile *gfile, int size){
	int ok = 1;

	if (gfile != NULL){

		gfile->data = malloc(sizeof(S_gyaml) * size);

		if (gfile->data != NULL){
			gfile->size = size;
			ok = 0;
		}
	}

	return ok;
}