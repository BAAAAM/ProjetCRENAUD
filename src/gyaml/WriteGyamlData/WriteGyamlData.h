#ifndef WRITEGYAMLDATA_H_INCLUDED
#define WRITEGYAMLDATA_H_INCLUDED

#include <stdio.h>

int WriteGyamlData(FILE *, char *);

/*
	Write a given data into a file
	Return 0 if success and 1 if failure
*/


#endif // WRITEGYAMLDATA_H_INCLUDED