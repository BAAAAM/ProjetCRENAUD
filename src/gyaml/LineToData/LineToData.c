/*
* @Author: klmp200
* @Date:   2016-05-06 23:24:39
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-12 15:57:15
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../S_gyaml.h"
#include "../../lists/list.h"

int LineToData(FILE *file, S_gyaml *collected){
	int ok = 0;
	char *tmpString = NULL;
	char character;
	int tmpSize;

	S_list *list = InitiateList();

	character = fgetc(file);
	while (character != '\n' && character != EOF){
		AppendList(list, &character, sizeof(char));
		character = fgetc(file);
	}

	if (character == '\n'){
		AppendList(list, &character, sizeof(char));
	}

	if (collected->data == NULL){

		collected->dataSize = list->size;
		collected->data = ConvertToStringList(list, 1);

	} else {

		tmpString = collected->data;

		collected->data = malloc(
			sizeof(char) * collected->dataSize + sizeof(char) * (list->size + 1));

		if (collected->data != NULL){

			collected->dataSize = collected->dataSize + list->size;

			strcpy(collected->data, tmpString);
			free(tmpString);

			tmpSize = collected->dataSize - list->size;

			tmpString = ConvertToStringList(list, 1);

			strcpy(collected->data + tmpSize, tmpString);
			free(tmpString);

		} else {
			collected->data = tmpString;
			ok = 1;
		}
	}

	FreeList(list);

	return ok;
}