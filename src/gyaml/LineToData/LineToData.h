#ifndef LINETODATA_H_INCLUDED
#define LINETODATA_H_INCLUDED

#include <stdlib.h>
#include "../S_gyaml.h"

int LineToData(FILE *, S_gyaml *);

/*
	Transform a line into data
	Go to the next line
	Return 0 if success and 1 if failure
*/

#endif // LINETODATA_H_INCLUDED