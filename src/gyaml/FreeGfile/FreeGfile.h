#ifndef FREEGFILE_H_INCLUDED
#define FREEGFILE_H_INCLUDED

#include "../S_gfile.h"

int FreeGfile(S_gfile *);

/*
	Free a gfile
	Return 0 if success and 1 if failure
*/

#endif // FREEGFILE_H_INCLUDED