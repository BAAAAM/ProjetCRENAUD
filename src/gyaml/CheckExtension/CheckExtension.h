#ifndef CHECKEXTENSION_H_INCLUDED
#define CHECKEXTENSION_H_INCLUDED

int CheckExtension(char[], int, char[], int);

/*
	Check if the first string have the same extension than the second one
	Need size of both strings
	Return 1 if matchs and 0 otherwise
*/

#endif // CHECKEXTENSION_H_INCLUDED