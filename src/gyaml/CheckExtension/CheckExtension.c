/*
* @Author: klmp200
* @Date:   2016-05-13 10:54:51
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-13 11:16:07
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int CheckExtension(char string[], int stringL, char ext[], int extL){

	int matchs = 1;
	int i = stringL;
	int j = extL;

	while (matchs && i >= 0 && string[i] != '.' && j >= 0){

		if (string[i] != ext[j]){
			matchs = 0;
		}

		i = i - 1;
		j = j - 1;
	}

	return matchs;

}