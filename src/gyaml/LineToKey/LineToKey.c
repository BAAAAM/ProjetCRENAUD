/*
* @Author: klmp200
* @Date:   2016-05-06 23:16:24
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-07 01:31:05
*/

#include <stdio.h>
#include <stdlib.h>
#include "../S_gyaml.h"
#include "../../lists/list.h"

int LineToKey(FILE *file, char character, S_gyaml *collected){
	int ok = 0;
	S_list *list = InitiateList();	

	while (character != ':' && character != '\n'
			&& character != EOF && collected->key == NULL){
		AppendList(list, &character, sizeof(char));
		character = fgetc(file);
	}

	if (character == ':' && collected->key == NULL){

		collected->keySize = list->size;
		collected->key = ConvertToStringList(list, 1);

		/* Go to the next line */

		while (character != '\n' && character != EOF){
			character = fgetc(file);
		}

	} else {
		if (collected->key != NULL){
			/* It's only because it's a new key */
			ok = 1;
			fseek(file, -1, SEEK_CUR);
		} else {
			/* The file is corrupted */
			ok = -1;
		}
	}

	FreeList(list);

	return ok;
}