#ifndef LINETOKEY_H_INCLUDED
#define LINETOKEY_H_INCLUDED

#include <stdlib.h>
#include "../S_gyaml.h"

int LineToKey(FILE *, char, S_gyaml *);

/*
	Transform a line into a key
	Need the last picked character
	Go to the next line
	Return 0 if success and 1 if failure
*/

#endif // LINETOKEY_H_INCLUDED