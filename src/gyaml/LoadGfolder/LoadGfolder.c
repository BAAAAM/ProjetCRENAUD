/*
* @Author: klmp200
* @Date:   2016-05-12 16:36:11
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-20 08:17:24
*/

#include <stdio.h>
#include <stdlib.h>

#include <dirent.h>
#include <string.h>

#ifndef _WIN32
	#include <sys/types.h>
#endif

#include "../S_gfile.h"
#include "../ParseFile/ParseFile.h"
#include "../CheckExtension/CheckExtension.h"
#include "../../lists/list.h"

S_list * LoadGfolder(char folder[]){

	S_list * list = NULL;
	S_gfile * gfile = NULL;
	DIR * rep = NULL;
	struct dirent * file = NULL;

	int fullPathSize;
	int folderSize;
	int fileNameSize;
	char *fullPath = NULL;

	rep = opendir(folder);

	if (rep != NULL){

		file = readdir(rep);

		list = InitiateList();

		folderSize = strlen(folder);

		while (file != NULL){

			fileNameSize = strlen(file->d_name);

			if (CheckExtension(file->d_name, fileNameSize, "godwin", 6) &&
					strcmp(file->d_name, "..") != 0 &&
						strcmp(file->d_name, ".") != 0){

				fullPathSize = folderSize + fileNameSize + 1;
				fullPath = malloc(sizeof(char) * fullPathSize);

				if (fullPath != NULL){

					strcpy(fullPath, folder);
					strcat(fullPath, file->d_name);

					gfile = ParseFile(fullPath);

					free(fullPath);

					if (gfile != NULL){

						AppendList(list, gfile, sizeof(S_gfile));
						free(gfile);

					}

					file = readdir(rep);

				} else {

					file = NULL;
					FreeList(list);
					list = NULL;

				}
			} else {

				file = readdir(rep);

			}
		}

		closedir(rep);
	} else {
		printf("Error while opening folder %s\n", folder);
	}

	return list;
}
