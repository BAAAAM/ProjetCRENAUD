#ifndef LOADGFOLDER_H_INCLUDED
#define LOADGFOLDER_H_INCLUDED

#include "../../lists/list.h"

S_list * LoadGfolder(char[]);

/*
	Load a folder and return a list of gfile
	Return NULL in case of failure
*/


#endif // LOADGFOLDER_H_INCLUDED