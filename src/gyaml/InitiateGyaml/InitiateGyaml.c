/*
* @Author: klmp200
* @Date:   2016-05-05 14:29:14
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-11 19:30:18
*/

#include <stdio.h>
#include <stdlib.h>

#include "../S_gyaml.h"

S_gyaml InitiateGyaml(){
	S_gyaml gyaml;

	gyaml.key = NULL;
	gyaml.data = NULL;

	gyaml.keySize = 0;
	gyaml.dataSize = 0;

	return gyaml;
}