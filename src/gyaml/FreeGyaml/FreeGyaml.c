/*
* @Author: klmp200
* @Date:   2016-05-05 01:32:18
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-11 21:36:39
*/

#include <stdio.h>
#include <stdlib.h>

#include "../S_gyaml.h"

void FreeGyaml(S_gyaml *gyaml){
	if (gyaml->key != NULL){
		free(gyaml->key);
	}
	if (gyaml->data != NULL){
		free(gyaml->data);
	}

	gyaml->keySize = 0;
	gyaml->dataSize = 0;
}