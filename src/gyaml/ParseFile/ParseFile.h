#ifndef PARSEFILE_H_INCLUDED
#define PARSEFILE_H_INCLUDED

#include "../S_gfile.h"

S_gfile * ParseFile(char[]);

/*
	Open a given file and return a S_gfile
	Return NULL in case of faliure
*/

#endif // PARSEFILE_H_INCLUDED