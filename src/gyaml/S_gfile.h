#ifndef SGFILE_H_INCLUDED
#define SGFILE_H_INCLUDED

#include "S_gyaml.h"

typedef struct S_gfile S_gfile;
struct S_gfile {
	S_gyaml * data;
	int size;
};

/*
	Contain an array of gyaml
	Size is at first undefined
*/

#endif // SGFILE_H_INCLUDED