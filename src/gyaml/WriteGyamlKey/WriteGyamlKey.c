/*
* @Author: klmp200
* @Date:   2016-05-19 16:13:56
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-19 16:30:10
*/

#include <stdio.h>
#include <stdlib.h>

#include "../S_gfile.h"
#include "../S_gyaml.h"

int WriteGyamlKey(FILE *file, char *string){
	int ok = 1;
	int i = 0;

	if (file != NULL && string != NULL){

		while (string[i] != '\0'){

			fputc(string[i], file);

			i++;

		}
		fputc(':', file);
		fputc('\n', file);
		ok = 0;
	}

	return ok;
}