#ifndef WRITEGYAMLKEY_H_INCLUDED
#define WRITEGYAMLKEY_H_INCLUDED

#include <stdio.h>

int WriteGyamlKey(FILE *, char *);

/*
	Write a given key into a file
	Return 0 if success and 1 if failure
*/

#endif // WRITEGYAMLKEY_H_INCLUDED