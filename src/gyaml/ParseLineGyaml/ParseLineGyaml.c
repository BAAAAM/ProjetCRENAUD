/*
* @Author: klmp200
* @Date:   2016-05-05 01:45:56
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-07 01:25:50
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../S_gyaml.h"
#include "../../lists/list.h"
#include "../LineToKey/LineToKey.h"
#include "../LineToData/LineToData.h"

int ParseLineGyaml(FILE *file, S_gyaml *collected){
	int ok = 0;
	char character;

	character = fgetc(file);

	if (character != '	'){
		/* That's mean it's a key */

		ok = LineToKey(file, character, collected);

	} else {
		
		/* It means it's data */

		ok = LineToData(file, collected);
	}

	return ok;

}