#ifndef PARSELINEGYAML_H_INCLUDED
#define PARSELINEGYAML_H_INCLUDED

#include <stdlib.h>
#include "../S_gyaml.h"

int ParseLineGyaml(FILE *, S_gyaml *);

/*
	Parse a gyaml line
	get a key or data and pour it in S_gyaml
	return 0 if success and 1 otherwise
	Can return -1 if the file is invalid
*/


#endif // PARSELINEGYAML_H_INCLUDED