#include <stdio.h>
#include <stdlib.h>


#include "../../wrestler/wrestler.h"
#include "../S_status.h"
#include "../StatusCpy/StatusCpy.h"


void Heal(S_wrestler *wrestler)
{
	int i=0;
	for(i=0;i<MAX_W_STATUS;i++){

		if(wrestler->status[i] != NULL){

			free(wrestler->status[i]);
			wrestler->status[i] = NULL;

		}
	}
}