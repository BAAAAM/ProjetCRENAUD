/*
* @Author: klmp200
* @Date:   2016-05-17 18:12:27
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-17 18:14:48
*/

#include <stdio.h>
#include <stdlib.h>

#include "../../lists/list.h"
#include "../S_status.h"
#include "../FreeStatus/FreeStatus.h"

int FreeStatusList(S_list *list){
	int ok = 1;

	if (list != NULL){
		ok = 0;
		while (list->first != NULL && ok != 1){
			ok = FreeStatus((S_status*)list->first->data);
			PopPtnList(list, list->first);
		}
		FreeList(list);
	}

	return ok;
}
