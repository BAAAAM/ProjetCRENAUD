#ifndef STATUSCPY_H_INCLUDED
#define STATUSCPY_H_INCLUDED

#include "../S_status.h"

S_status * StatusCpy(S_status *);

/*
	Copy a given status and return it
	Return NULL in case of error
*/

#endif // STATUSCPY_H_INCLUDED