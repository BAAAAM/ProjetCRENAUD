#ifndef SSTATUS_H_INCLUDED
#define SSTATUS_H_INCLUDED

typedef struct S_status S_status;
struct S_status{
	char *name;
	int mMoney;
	int bFame;
	int bStrength;
	int rTime;
	S_status *sideEffect;

	char *sideEffectName;
};

#endif // SSTATUS_H_INCLUDED