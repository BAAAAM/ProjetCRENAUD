/*
* @Author: klmp200
* @Date:   2016-05-17 18:04:09
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-17 18:08:22
*/

#include <stdio.h>
#include <stdlib.h>

#include "../S_status.h"

int FreeStatus(S_status *status){
	int ok = 1;

	if (status != NULL){

		if (status->name != NULL){
			free(status->name);
			status->name = NULL;
		}

		if (status->sideEffectName != NULL){
			free(status->sideEffectName);
			status->sideEffectName = NULL;
		}

		status->sideEffect = NULL;

		status->mMoney = 0;
		status->bFame = 0;
		status->bStrength = 0;
		status->rTime = 0;

		ok = 0;
	}

	return ok;
}	