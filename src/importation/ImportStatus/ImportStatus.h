#ifndef IMPORTSTATUS_H_INCLUDED
#define IMPORTSTATUS_H_INCLUDED

#include "../../lists/list.h"

S_list * ImportStatus(char[]);

/*
	Create a list of status from a givent folder
	Return NULL in case of failure
*/

#endif // IMPORTSTATUS_H_INCLUDED