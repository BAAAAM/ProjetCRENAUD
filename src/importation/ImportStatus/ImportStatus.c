/*
* @Author: klmp200
* @Date:   2016-05-18 12:34:54
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-27 12:00:03
*/

#include <stdio.h>
#include <stdlib.h>

#include "../../lists/list.h"
#include "../../gyaml/gyaml.h"
#include "../../status/status.h"
#include "../FindSideEffectStatus/FindSideEffectStatus.h"
#include "../GfileToStatus/GfileToStatus.h"

S_list * ImportStatus(char folder[]){
	S_list *list = LoadGfolder(folder);
	S_list *statusList = NULL;
	S_element *element = NULL;
	S_status status;
	S_status *statusPtn = NULL;

	if (list != NULL){
		statusList = InitiateList();

		while (list->first != NULL){
			status = GfileToStatus(((S_gfile*)list->first->data));
			AppendList(statusList, &status, sizeof(S_status));
			PopPtnList(list, list->first);
		}

		FreeList(list);

		/* Importation of side effects */

		element = statusList->first;

		while (element != NULL){
			statusPtn = ((S_status*)element->data);

			if (statusPtn->sideEffectName != NULL){

				FindSideEffectStatus(statusList, statusPtn);

			}

			element = element->next;
		}

	} else {
		printf("Error while importing status");
	}

	return statusList;
}