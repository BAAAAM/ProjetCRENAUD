#ifndef FINDSIDEEFFECTSTATUS_H_INCLUDED
#define FINDSIDEEFFECTSTATUS_H_INCLUDED

#include "../../lists/list.h"
#include "../../status/status.h"

int FindSideEffectStatus(S_list *, S_status *);

/*
	Look for the side effect of a status into a given list
	Return 1 if a side effect is found and 0 otherwise
*/


#endif // FINDSIDEEFFECTSTATUS_H_INCLUDED