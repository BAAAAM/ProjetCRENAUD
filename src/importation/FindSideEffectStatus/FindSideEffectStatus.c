/*
* @Author: klmp200
* @Date:   2016-05-18 13:14:58
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-19 08:07:22
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../../lists/list.h"
#include "../../status/status.h"

int FindSideEffectStatus(S_list *list, S_status *status){
	int found = 0;
	S_element *element = NULL;
	S_status *statusCheck = NULL;

	if (list != NULL && status->sideEffectName != NULL){

		element = list->first;

		while (element != NULL && found == 0){

			statusCheck = ((S_status*)element->data);

			if (statusCheck->name != NULL){

				found = (strcmp(status->sideEffectName, statusCheck->name) == 0);

			}

			if (found){

				status->sideEffect = statusCheck;
				free(status->sideEffectName);
				status->sideEffectName = NULL;
				found = 1;

			} else {

				element = element->next;

			}

		}
	}

	if (found == 0){

		free(status->sideEffectName);
		status->sideEffectName = NULL;

	}

	return found;
}