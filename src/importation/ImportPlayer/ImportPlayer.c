/*
* @Author: klmp200
* @Date:   2016-05-26 15:12:44
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-26 15:33:49
*/

#include <stdio.h>
#include <stdlib.h>

#include "../../player/player.h"
#include "../../gyaml/gyaml.h"
#include "../GfileToPlayer/GfileToPlayer.h"

S_player ImportPlayer(char file[]){

	S_gfile *gfile = ParseFile(file);
	S_player player;

	if (gfile != NULL){

		player = GfileToPlayer(gfile);
		FreeGfile(gfile);

	} else {

		player = InitiatePlayer("", 0, 0);

	}

	return player;
}