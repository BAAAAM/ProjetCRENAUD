/*
* @Author: klmp200
* @Date:   2016-05-20 08:38:47
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-20 09:50:55
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void EndAtLast(char *string, int c){
	char *pos = NULL;

	if (string != NULL){

		pos = strrchr(string, c);
		
	}

	if (pos != NULL){

		pos[0] = '\0';

	}
}