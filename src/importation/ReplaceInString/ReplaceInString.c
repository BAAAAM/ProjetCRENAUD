/*
* @Author: klmp200
* @Date:   2016-05-18 12:55:58
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-18 13:38:23
*/

#include <stdio.h>
#include <stdlib.h>

void ReplaceInString(char string[], char toDelete, char toUse){
	int i = 0;

	if (string != NULL){
		while (string[i] != '\0'){
			if (string[i] == toDelete){
				string[i] = toUse;
			}
			i++;
		}
	}
}