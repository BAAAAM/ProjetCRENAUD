/*
* @Author: klmp200
* @Date:   2016-05-16 18:11:03
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-27 12:00:45
*/

#include <stdio.h>
#include <stdlib.h>

#include "../../lists/list.h"
#include "../../gyaml/gyaml.h"
#include "../../wrestler/wrestler.h"
#include "../GfileToWrestler/GfileToWrestler.h"

S_list * ImportWrestlers(char folder[], S_list *listStatus){
	S_list *list = LoadGfolder(folder);
	S_list *wrestlerList = NULL;
	S_wrestler wrestler;

	if (list != NULL){

		wrestlerList = InitiateList();
		
		while (list->first != NULL){
			wrestler = GfileToWrestler(((S_gfile*)list->first->data), listStatus);
			AppendList(wrestlerList, &wrestler, sizeof(S_wrestler));
			PopPtnList(list, list->first);
		}

		FreeList(list);

	} else {
		printf("Error while importing wrestlers");
	}

	return wrestlerList;

}