/*
* @Author: klmp200
* @Date:   2016-05-13 14:59:32
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-21 18:56:48
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../ReplaceInString/ReplaceInString.h"
#include "../FindStatusWrestler/FindStatusWrestler.h"
#include "../EndAtLast/EndAtLast.h"
#include "../../lists/list.h"
#include "../../wrestler/wrestler.h"
#include "../../gyaml/gyaml.h"

S_wrestler GfileToWrestler(S_gfile *gfile, S_list *listStatus){
	S_wrestler wrestler = InitiateWrestler();
	S_list *tmpList = NULL;

	char *statusString = NULL;

	int nbL = 0;
	int j = 0;
	int i;

	for (i=0;i < gfile->size;i++){
		if (strcmp(gfile->data[i].key, "name") == 0){

			wrestler.name = gfile->data[i].data;
			gfile->data[i].data = NULL;

			EndAtLast(wrestler.name, '\n');
			ReplaceInString(wrestler.name, '\n', ' ');

		} else if (strcmp(gfile->data[i].key, "description") == 0){

			wrestler.description = gfile->data[i].data;
			gfile->data[i].data = NULL;

			EndAtLast(wrestler.description, '\n');

		} else if (strcmp(gfile->data[i].key, "ascii") == 0){

			wrestler.ascii = gfile->data[i].data;
			gfile->data[i].data = NULL;

		} else if (strcmp(gfile->data[i].key, "price") == 0){

			wrestler.price = atoi(gfile->data[i].data);

		} else if (strcmp(gfile->data[i].key, "fame") == 0){

			wrestler.fame = atoi(gfile->data[i].data);

		} else if (strcmp(gfile->data[i].key, "strength") == 0){

			wrestler.strength = atoi(gfile->data[i].data);

		} else if (strcmp(gfile->data[i].key, "status") == 0){

			tmpList = InitiateList();

			while (gfile->data[i].data[j] != '\0' && nbL < MAX_W_STATUS){

				if (gfile->data[i].data[j] == '\n'){

					statusString = ConvertToStringList(tmpList, 1);
					wrestler.status[nbL] = FindStatusWrestler(listStatus, statusString);
					free(statusString);
					statusString = NULL;
					nbL++;

				} else {

					AppendList(tmpList, &gfile->data[i].data[j], sizeof(char));

				}

				j++;

			}

			FreeList(tmpList);

		}

		FreeGyaml(&gfile->data[i]);
	}

	free(gfile->data);

	return wrestler;
}