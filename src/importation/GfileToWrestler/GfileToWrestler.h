#ifndef GFILETOWRESTLER_H_INCLUDED
#define GFILETOWRESTLER_H_INCLUDED

#include "../../wrestler/wrestler.h"
#include "../../lists/list.h"
#include "../../gyaml/gyaml.h"

S_wrestler GfileToWrestler(S_gfile *, S_list *);

/*
	Convert a gfile into a wrestler
	Need a list of status
*/

#endif // GFILETOWRESTLER_H_INCLUDED