/*
* @Author: klmp200
* @Date:   2016-05-26 14:58:59
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-31 10:19:56
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../ReplaceInString/ReplaceInString.h"
#include "../EndAtLast/EndAtLast.h"
#include "../../player/player.h"
#include "../../gyaml/gyaml.h"

S_player GfileToPlayer(S_gfile *gfile){
	S_player player = InitiatePlayer("", 0, 0);
	int i;
	int j;

	for (i=0; i<gfile->size; i++){

		if (strcmp(gfile->data[i].key, "name") == 0){

			for (j=0; j<PLAYER_NAME_MAX - 1; j++){

				player.name[j] = gfile->data[i].data[j];

			}
			player.name[PLAYER_NAME_MAX - 1] = '\0';

			EndAtLast(player.name, '\n');
			ReplaceInString(player.name, '\n', ' ');

		} else if (strcmp(gfile->data[i].key, "money") == 0){

			player.money = atoi(gfile->data[i].data);

		} else if (strcmp(gfile->data[i].key, "fame") == 0){


			player.fame = atoi(gfile->data[i].data);

		} else if (strcmp(gfile->data[i].key, "current year") == 0){

			player.currentYear = atoi(gfile->data[i].data);

		} else if (strcmp(gfile->data[i].key, "current round") == 0){


			player.currentRound = atoi(gfile->data[i].data);

		}

		FreeGyaml(&gfile->data[i]);

	}

	free(gfile->data);
	gfile->data = NULL;	

	return player;
}