#ifndef FINDSTATUSWRESTLER_H_INCLUDED
#define FINDSTATUSWRESTLER_H_INCLUDED

#include "../../lists/list.h"
#include "../../status/status.h"

S_status * FindStatusWrestler(S_list *, char[]);

/*
	Look for given status in a given list
	Return a status or NULL
*/

#endif // FINDSTATUSWRESTLER_H_INCLUDED