/*
* @Author: klmp200
* @Date:   2016-05-21 15:58:08
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-22 00:33:35
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../../lists/list.h"
#include "../../status/status.h"

S_status * FindStatusWrestler(S_list *statusList, char string[]){
	int found = 0;
	S_status *status = NULL;
	S_element *element = NULL;

	if (statusList != NULL && string[0] != '\0'){

		element = statusList->first;
		while (element != NULL && found == 0){

			status = ((S_status*)element->data);

			if (status->name != NULL){

				found = (strcmp(string, status->name) == 0);

			}

			element = element->next;

		}

		if (!found){

			status = NULL;

		} else {

			status = StatusCpy(status);	
			
		}

	}

	return status;
}