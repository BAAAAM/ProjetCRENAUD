/*
* @Author: klmp200
* @Date:   2016-06-01 01:24:18
* @Last Modified by:   klmp200
* @Last Modified time: 2016-06-01 01:29:50
*/

#include <stdio.h>
#include <stdlib.h>

#include "../../game/game.h"
#include "../../player/player.h"
#include "../../wrestler/wrestler.h"
#include "../../status/status.h"
#include "../../gyaml/gyaml.h"
#include "../SavePlayer/SavePlayer.h"
#include "../SaveList/SaveList.h"

void SaveGame(S_game game){

	if (game.mode){

		/* Save for arcade mode */

		EmptyGfolder("data/save/arcade/wrestlers/");
		EmptyGfolder("data/save/arcade/status/");

		SavePlayer("data/save/arcade/player.godwin", game.player);
		SaveWrestlers("data/save/arcade/wrestlers/", game.wList);
		SaveStatus("data/save/arcade/status/", game.sList);


	} else {

		/* Save to adventure mode */

		EmptyGfolder("data/save/adventure/wrestlers/");
		EmptyGfolder("data/save/adventure/status/");

		SavePlayer("data/save/adventure/player.godwin", game.player);
		SaveWrestlers("data/save/adventure/wrestlers/", game.wList);
		SaveStatus("data/save/adventure/status/", game.sList);

	}

}