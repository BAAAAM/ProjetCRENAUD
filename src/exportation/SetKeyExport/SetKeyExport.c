/*
* @Author: klmp200
* @Date:   2016-05-18 19:40:06
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-18 19:45:25
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../../gyaml/gyaml.h"

void SetKeyExport(S_gyaml *gyaml, char key[], int keySize){

	gyaml->key = malloc(sizeof(char) * keySize);

	if (gyaml->key != NULL){

		gyaml->keySize = keySize;
		gyaml->key[0] = '\n';
		strcpy(gyaml->key, key);

	}
}
