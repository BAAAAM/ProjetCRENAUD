/*
* @Author: klmp200
* @Date:   2016-05-27 09:16:19
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-27 09:22:25
*/

#include <stdio.h>
#include <stdlib.h>

#include "../../lists/list.h"
#include "../../status/status.h"

char * ElementToStatusName(S_element *element){

	return ((S_status*)element->data)->name;	

}