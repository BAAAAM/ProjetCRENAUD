#ifndef ELEMENTTOSTATUSNAME_H_INCLUDED
#define ELEMENTTOSTATUSNAME_H_INCLUDED

#include "../../lists/list.h"

char * ElementToStatusName(S_element *);

/*
	Convert an element into a status name
*/

#endif // ELEMENTTOSTATUSNAME_H_INCLUDED