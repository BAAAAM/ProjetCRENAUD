#ifndef SAVEPLAYER_H_INCLUDED
#define SAVEPLAYER_H_INCLUDED

#include "../../player/player.h"

int SavePlayer(char [], S_player);

/*
	Save a player into a given file path
	return 0 if success and 1 in case of failure
*/

#endif // SAVEPLAYER_H_INCLUDED