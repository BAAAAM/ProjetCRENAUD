/*
* @Author: klmp200
* @Date:   2016-05-26 16:47:14
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-27 08:55:20
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../../lists/list.h"
#include "../../wrestler/wrestler.h"
#include "../../gyaml/gyaml.h"
#include "../../importation/importation.h"
#include "../WrestlerToGfile/WrestlerToGfile.h"

int SaveList(char path[], S_list *list, char *(*CastName)(S_element *), S_gfile *(*ConvertGfile)(S_element *)){

	int ok = 0;
	int fullPathSize;
	int pathSize = strlen(path);
	char *fullPath = NULL;
	S_element *element = NULL;
	S_gfile *gfile = NULL; 

	if (list != NULL){

		element = list->first;

		while (element != NULL && ok == 0){

			fullPathSize = pathSize + strlen((*CastName)(element)) + 9;
			fullPath = malloc(sizeof(char) * fullPathSize);

			if (fullPath != NULL){

				strcpy(fullPath, path);
				strcat(fullPath, (*CastName)(element));
				strcat(fullPath, ".godwin");

				ReplaceInString(fullPath, ' ', '_');

				gfile = (*ConvertGfile)(element);
				ok = WriteGfile(fullPath, gfile);
				FreeGfile(gfile);

				free(fullPath);

				element = element->next;

			} else {

				ok = 1;

			}

		}

	} else {

		ok = 1;

	}

	return ok;
}