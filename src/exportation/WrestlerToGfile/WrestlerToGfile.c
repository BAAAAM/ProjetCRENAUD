/*
* @Author: klmp200
* @Date:   2016-05-18 15:48:44
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-27 14:20:43
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../../wrestler/wrestler.h"
#include "../../gyaml/gyaml.h"
#include "../SetKeyExport/SetKeyExport.h"
#include "../SetDataExport/SetDataExport.h"
#include "../IntToString/IntToString.h"

S_gfile * WrestlerToGfile(S_wrestler * wrestler){
	S_gfile *gfile = InitiateGfile();
	S_gyaml gyaml;
	int statusSize = 1;
	int i;
	char *nbString = NULL;

	if (SetSizeGfile(gfile, 7) == 0){


			/* Convert name */

			gyaml = InitiateGyaml();
			SetKeyExport(&gyaml, "name", 5);
			SetDataExport(&gyaml, wrestler->name);
			memcpy(&gfile->data[0], &gyaml, sizeof(S_gyaml));

			/* Convert description */

			gyaml = InitiateGyaml();
			SetKeyExport(&gyaml, "description", 12);
			SetDataExport(&gyaml, wrestler->description);
			memcpy(&gfile->data[1], &gyaml, sizeof(S_gyaml));

			/* Convert price */

			gyaml = InitiateGyaml();
			SetKeyExport(&gyaml, "price", 6);
			nbString = IntToString(wrestler->price);
			SetDataExport(&gyaml, nbString);
			free(nbString);
			memcpy(&gfile->data[2], &gyaml, sizeof(S_gyaml));

			/* Convert fame */

			gyaml = InitiateGyaml();
			SetKeyExport(&gyaml, "fame", 5);
			nbString = IntToString(wrestler->fame);
			SetDataExport(&gyaml, nbString);
			free(nbString);
			memcpy(&gfile->data[3], &gyaml, sizeof(S_gyaml));


			/* Convert strength */

			gyaml = InitiateGyaml();
			SetKeyExport(&gyaml, "strength", 9);
			nbString = IntToString(wrestler->strength);
			SetDataExport(&gyaml, nbString);
			free(nbString);
			memcpy(&gfile->data[4], &gyaml, sizeof(S_gyaml));


			/* Convert status */

			gyaml = InitiateGyaml();
			SetKeyExport(&gyaml, "status", 7);

			for (i=0;i<MAX_W_STATUS;i++){

				if (wrestler->status[i] != NULL){

					statusSize = statusSize + strlen(wrestler->status[i]->name);

				}

			}

			if (statusSize != 1){

				nbString = malloc(sizeof(char) * statusSize);

			} else {

				nbString = NULL;

			}

			if (nbString != NULL){

				nbString[0] = '\0';

				for (i=0;i<MAX_W_STATUS;i++){

					if (wrestler->status[i] != NULL){

						strcat(nbString, wrestler->status[i]->name);
						strcat(nbString, "\n");

					}

				}
		
				SetDataExport(&gyaml, nbString);
				free(nbString);

			} else {
				SetDataExport(&gyaml, "\0");
			}

			memcpy(&gfile->data[5], &gyaml, sizeof(S_gyaml));

			/* Convert ascii */

			gyaml = InitiateGyaml();
			SetKeyExport(&gyaml, "ascii", 6);
			SetDataExport(&gyaml, wrestler->ascii);
			memcpy(&gfile->data[6], &gyaml, sizeof(S_gyaml));

	}

	return gfile;
}