#ifndef PLAYERTOFILE_H_INCLUDED
#define PLAYERTOFILE_H_INCLUDED

#include "../../player/player.h"
#include "../../gyaml/gyaml.h"

S_gfile * PlayerToGfile(S_player *);

/*
	Convert a wrestler into a gfile
	Return null in case of failure
*/


#endif // PLAYERTOFILE_H_INCLUDED