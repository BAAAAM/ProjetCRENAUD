/*
* @Author: klmp200
* @Date:   2016-05-18 19:56:41
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-20 09:57:00
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../../gyaml/gyaml.h"
#include "../../importation/importation.h"

void SetDataExport(S_gyaml *gyaml, char *string){
	char *newString = NULL;

	if (string != NULL){

		gyaml->dataSize = strlen(string);

		newString = malloc(sizeof(char) * (gyaml->dataSize + 1));

		if (newString != NULL){

			strcpy(newString, string);

		}


	} else {

		gyaml->dataSize = 0;

	}

	gyaml->data = newString;

}
