#ifndef SETDATAEXPORT_H_INCLUDED
#define SETDATAEXPORT_H_INCLUDED

#include "../../gyaml/gyaml.h"

void SetDataExport(S_gyaml *, char *);

/*
	Set data into a gyaml with a given string
*/

#endif // SETDATAEXPORT_H_INCLUDED