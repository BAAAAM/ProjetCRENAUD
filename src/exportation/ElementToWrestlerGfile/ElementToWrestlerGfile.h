#ifndef ELEMENTTOWRESTLERGFILE_H_INCLUDED
#define ELEMENTTOWRESTLERGFILE_H_INCLUDED

S_gfile * ElementToWrestlerGfile(S_element *);

/*
	Convert an element into a gfile of a wrestler
*/

#endif // ELEMENTTOWRESTLERGFILE_H_INCLUDED