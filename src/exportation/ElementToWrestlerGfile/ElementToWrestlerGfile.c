/*
* @Author: klmp200
* @Date:   2016-05-27 08:52:11
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-27 08:53:19
*/

#include <stdio.h>
#include <stdlib.h>

#include "../../lists/list.h"
#include "../../wrestler/wrestler.h"
#include "../WrestlerToGfile/WrestlerToGfile.h"

S_gfile * ElementToWrestlerGfile(S_element *element){

	return WrestlerToGfile((S_wrestler*)element->data);
	
}
