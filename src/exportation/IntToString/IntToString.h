#ifndef INTTOSTRING_H_INCLUDED
#define INTTOSTRING_H_INCLUDED

char * IntToString(int);

/*
	Convert an integer into a string
	The size is dynamic
	Return NULL in case of error
*/

#endif // INTTOSTRING_H_INCLUDED