#ifndef SPLAYER_H_INCLUDED
#define SPLAYER_H_INCLUDED

#define PLAYER_NAME_MAX 30
#define ROUNDS_PER_YEAR 5
#define MAX_YEARS 12
#define MAX_COMPET 8

typedef struct S_player S_player;
struct S_player {
	char name[PLAYER_NAME_MAX];
	int money;
	int fame;
	int currentYear;
	int currentRound;
};

#endif // SPLAYER_H_INCLUDED