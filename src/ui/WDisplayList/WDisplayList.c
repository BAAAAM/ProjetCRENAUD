/*
* @Author: klmp200
* @Date:   2016-05-23 22:12:51
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-25 14:01:36
*/

#include <stdio.h>
#include <stdlib.h>

#include "../getcurses.h"
#include "../../lists/list.h"
#include "../ProcessPosition/ProcessPosition.h"

S_element * WDisplayList(WINDOW *win, int *n, S_list *list, char *(*cast)(S_element *)){
	int i = 0;
	int limit = 0;
	int yMax;
	S_element *element = NULL;
	S_element *choosenElement = NULL;

	if (list != NULL){

		scrollok(win, TRUE);

		wclear(win);
		wmove(win, 0, 0);
		wrefresh(win);

		yMax = getmaxy(win);

		*n = ProcessPosition(*n, list->size);

		if (*n >= yMax / 2 && list->size > yMax){

			limit = yMax / 2 + *n - 1;

		} else {

			limit = yMax - 1;

		}

		element = list->first;

		while (element != NULL && i < limit){

			if (*n == i){

				choosenElement = element;
				wattron(win, A_REVERSE);
				wprintw(win, "%s\n", (*cast)(element));
				wattroff(win, A_REVERSE);

			} else {

				wprintw(win, "%s\n", (*cast)(element));

			}

			i++;
			element = element->next;

		}

		wrefresh(win);

	} else {

		*n = -1;

	}

	return choosenElement;
}