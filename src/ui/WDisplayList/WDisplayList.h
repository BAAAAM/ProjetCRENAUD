#ifndef WDISPLAYLLIST_H_INCLUDED
#define WDISPLAYLLIST_H_INCLUDED

#include "../../lists/list.h"

#define DisplayList(n, list, cast) (WDisplayWrestlers(stdscr, n, list, cast))

S_element * WDisplayList(WINDOW *, int *, S_list *, char *(*)(S_element *));

/*
	Display a given list and return the number of the element selected
	Need a window, a positon of cursor in the list and the list
	Also need a cast function
	Return an element

	DisplayList is a macro to print in stdscr
*/

#endif // WDISPLAYLLIST_H_INCLUDED