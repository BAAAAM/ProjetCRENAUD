#ifndef GETCURSES_H_INCLUDED
#define GETCURSES_H_INCLUDED

#ifndef _WIN32
	#include <ncurses.h>
#else
	#include <curses.h>
#endif

#endif // GETCURSES_H_INCLUDED