/*
* @Author: klmp200
* @Date:   2016-05-26 13:04:24
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-28 17:08:21
*/

#include <stdio.h>
#include <stdlib.h>

#include "../getcurses.h"
#include "../../player/player.h"

void WGetName(WINDOW *win, S_player *player){

	int yMax, xMax;

	getmaxyx(win, yMax, xMax);
	wclear(win);

	echo();
	curs_set(1);

	mvwprintw(win, (yMax - 2) / 2, (xMax - 38) / 2, "Entrez le nom de votre ligue de catch");
	wmove(win, (yMax - 2) / 2 + 2, (xMax - 38) / 2);

	wgetnstr(win, player->name, PLAYER_NAME_MAX - 1);
	wrefresh(win);
	wclear(win);

	wrefresh(win);
}