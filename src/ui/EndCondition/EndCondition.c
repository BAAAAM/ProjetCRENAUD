/*
* @Author: klmp200
* @Date:   2016-05-31 13:09:27
* @Last Modified by:   klmp200
* @Last Modified time: 2016-06-02 17:33:08
*/

#include <stdio.h>
#include <stdlib.h>

#include "../../game/game.h"

int EndCondition(S_game game){

	int value;

	if (game.mode){

		value = game.save == 0 && !game.victory;

	} else {

		value = game.player.currentYear < MAX_YEARS && !game.victory && game.save == 0;

	}

	return value;
}