#ifndef ENDCONDITION_H_INCLUDED
#define ENDCONDITION_H_INCLUDED

#include "../../game/game.h"

int EndCondition(S_game);

/*
	Return condition for the game to end
*/

#endif // ENDCONDITION_H_INCLUDED