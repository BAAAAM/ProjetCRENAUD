/*
* @Author: klmp200
* @Date:   2016-06-02 08:10:35
* @Last Modified by:   klmp200
* @Last Modified time: 2016-06-03 01:21:28
*/

#include <stdio.h>
#include <stdlib.h>

#include "../getcurses.h"
#include "../../game/game.h"

void DisplayScoring(S_player *compet[]){

	int i;
	WINDOW *centered;

	initscr();
	noecho();
	clear();
	curs_set(0);

	centered = subwin(stdscr, MAX_COMPET, PLAYER_NAME_MAX + 20, (LINES - MAX_COMPET) / 2, (COLS - PLAYER_NAME_MAX - 20) / 2);

	mvprintw((LINES - MAX_COMPET) / 2 - 2, (COLS - 21) / 2, "Scores de la saison :");

	wmove(centered, 0, 0);

	for (i = MAX_COMPET - 1; i >= 0; i--){

		wprintw(centered, "%s avec %d de popularite\n", compet[i]->name, compet[i]->fame);

	}

	getch();

	delwin(centered);
	endwin();

}