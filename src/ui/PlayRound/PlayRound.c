/*
* @Author: klmp200
* @Date:   2016-05-28 14:09:47
* @Last Modified by:   klmp200
* @Last Modified time: 2016-06-01 15:44:12
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../getcurses.h"
#include "../WDisplayList/WDisplayList.h"
#include "../../lists/list.h"
#include "../../player/player.h"
#include "../../wrestler/wrestler.h"
#include "../../status/status.h"
#include "../WDisplayList/WDisplayList.h"
#include "../../wrestler/ElementToWrestlerName/ElementToWrestlerName.h"
#include "../DisplayWStatus/DisplayWStatus.h"

int PlayRound(S_list *wList, S_list *sList, S_player *player){

	S_list *selectedList = InitiateList();

	int save = 0;

	char *years[] = {
		"du Grand Ragondin Cosmique",
		"du Kiwi de l'Amour",
		"de Loth L'axolotl",
		"du Hanneton Sacré Ancestral",
		"de la Wyvern Flamboyante",
		"la Drosophile De La Troisième Corde",
		"du Blobfish de la vengeance",
		"du Cochon De Mer Rouge",
		"du Aye-Aye de l'enfer",
		"du Manchot de la Sagesse",
		"du Roi Tanuki",
		"du Caribou des Plaines",
	};

	int pos = 0;

	WINDOW *wmenu, *wmenusub, *wmenuMatch, *wmenuTuto;
	int startx, starty, width, height;
	int ch;

	S_element *element = NULL;

	width = 30;
	startx = 0;
	starty = 0;
	height = LINES;

	wmenu = newwin(height, width, starty, startx);
	wmenusub = subwin(wmenu, height -2, width - 2, starty + 1, startx + 1);
	wmenuMatch = newwin(height, width, starty, startx + width);
	wmenuTuto = newwin(10, COLS - (width * 2), LINES - 10, width * 2);

	scrollok(wmenusub, TRUE);

	wborder(wmenu, '|', '|', '-', '-', '+', '+', '+', '+');
	wborder(wmenuMatch, ' ', '|', '-', '-', '-', '+', '-', '+');
	wborder(wmenuTuto, '|', '|', '-', '-', '+', '+', '+', '+');

	initscr();

	refresh();
	wrefresh(wmenu);
	wrefresh(wmenuMatch);
	wrefresh(wmenuTuto);
	wrefresh(wmenuTuto);

	cbreak();
	keypad(stdscr, TRUE);
	curs_set(0);
	noecho();

	start_color();
	init_pair(3, COLOR_RED, COLOR_BLACK);
	init_pair(4, COLOR_GREEN, COLOR_BLACK);

	mvprintw(LINES - 8, width * 2 + 2, "Choisissez en premier le catcheur gagnant");
	mvprintw(LINES - 7, width * 2 + 2, "Choisissez ensuite le perdant");
	mvprintw(LINES - 5, width * 2 + 2, "Utilisez ECHAPE pour quitter");
	mvprintw(LINES - 4, width * 2 + 2, "Utilisez F1 pour quitter et sauvegarder");
	mvprintw(LINES - 3, width * 2 + 2, "Utilisez F2 pour soigner un catcheur");
	mvprintw(LINES - 2, width * 2 + 2, "Soigner coute 300 Eurofrancs");

	element = wList->first;

	DisplayWStatus((S_wrestler*)element->data, 3, width * 2 + 2);

	move(5 + MAX_W_STATUS, width * 2 + 2);
	clrtoeol();
	printw("Vous avez %d Eurofrancs", player->money);

	move(6 + MAX_W_STATUS, width * 2 + 2);
	clrtoeol();
	printw("Votre popularite est de %d", player->fame);

	mvprintw(LINES - 13, width * 2 + 2, "Annee %s", years[player->currentYear % MAX_YEARS]);
	mvprintw(LINES - 12, width * 2 + 2, "Il vous reste %d matchs avant l'annee prochaine", ROUNDS_PER_YEAR - player->currentRound);

	WDisplayList(wmenusub, &pos, wList, (ElementToWrestlerName));

	while (selectedList->size < 2 && (ch = getch()) != 27 && ch != KEY_F(1)){

		switch (ch){

			case KEY_UP:
				pos--;
				break;

			case KEY_DOWN:
				pos++;
				break;

			case KEY_F(2):
				element = WDisplayList(wmenusub, &pos, wList, (ElementToWrestlerName));
				if (player->money >= 300){
					Heal((S_wrestler*)element->data);
					player->money = player->money - 300;
				}
				break;

		}

		element = WDisplayList(wmenusub, &pos, wList, (ElementToWrestlerName));

		if (ch == '\n'){

			AppendList(selectedList, element->data, sizeof(S_wrestler));
			PopPtnList(wList, element);

		}

		element = WDisplayList(wmenusub, &pos, wList, (ElementToWrestlerName));

		DisplayWStatus((S_wrestler*)element->data, 3, width * 2 + 2);

		element = selectedList->first;

		if (element != NULL){

			mvprintw(starty + 1, startx + width + 1, "%s", ((S_wrestler*)element->data)->name);
			element = element->next;

		}

		move(5 + MAX_W_STATUS, width * 2 + 2);
		clrtoeol();
		printw("Vous avez %d Eurofrancs", player->money);

		move(6 + MAX_W_STATUS, width * 2 + 2);
		clrtoeol();
		printw("Votre popularite est de %d", player->fame);


	}

	if (selectedList->size == 2){

		FameC(*(S_wrestler*)selectedList->first->data,
			*(S_wrestler*)selectedList->first->next->data, player);

		ApplyRandomEffect(sList, (S_wrestler*)selectedList->first->data);
		ApplyRandomEffect(sList, (S_wrestler*)selectedList->first->next->data);

		player->currentRound = player->currentRound + 1;

	}

	delwin(wmenusub);
	delwin(wmenu);
	delwin(wmenuMatch);
	delwin(wmenuTuto);

	endwin();

	if (ch == KEY_F(1)){

		save = 1;

	} else if (ch == 27){

		save = -1;

	}

	wList = ConcatFastWrestlerList(wList, selectedList);

	return save;
}
