#ifndef WRESTLERSELECTION_H_INCLUDED
#define WRESTLERSELECTION_H_INCLUDED

#include "../../lists/list.h"
#include "../../player/player.h"

S_list * WrestlerSelection(S_list *, S_player *);

/*
	Wrestler selection menu, need a list of wrestlers and a player
	Modify player given (money principaly)
	Return a list of selected wrestlers
*/

#endif // WRESTLERSELECTION_H_INCLUDED