/*
* @Author: klmp200
* @Date:   2016-05-23 22:48:03
* @Last Modified by:   klmp200
* @Last Modified time: 2016-06-02 17:24:11
*/

#include <stdio.h>
#include <stdlib.h>

#include "../../lists/list.h"
#include "../../wrestler/wrestler.h"
#include "../../player/player.h"

#include "../getcurses.h"
#include "../WDisplayList/WDisplayList.h"
#include "../WrestlerIdentity/WrestlerIdentity.h"
#include "../../wrestler/ElementToWrestlerName/ElementToWrestlerName.h"

S_list * WrestlerSelection(S_list *wList, S_player *player){

	int ch;
	int pos = 0;
	S_list *selectedList = InitiateList();

	S_element *element = NULL;

	WINDOW *wmenu, *wmenusub, *title1, *playerData;
	int startx, starty, width, height;

	initscr();

	cbreak();
	keypad(stdscr, TRUE);
	curs_set(0);
	noecho();

	start_color();
	init_pair(3, COLOR_RED, COLOR_BLACK);

	width = 30;
	startx = 0;
	starty = 3;
	height = LINES - starty;

	title1 = newwin(height, width, 0, 0);

	playerData = newwin(10, COLS - width, LINES - 10, width);

	wmenu = newwin(height, width, starty, startx);
	wmenusub = subwin(wmenu, height - 2, width - 2, starty + 1, startx + 1);

	scrollok(wmenusub, TRUE);

	wborder(title1, '|', '|', '-', '-', '+', '+', '|', '|');
	wborder(wmenu, '|', '|', '-', '-', '|', '|', '+', '+');
	wborder(playerData, '|', '|', '-', '-', '+', '+', '+', '+');

	mvwprintw(title1, 1, 2, "Selectionnez vos catcheurs");

	mvwprintw(playerData, 1, 2, "Informations de la ligue %s", player->name);
	mvwprintw(playerData, 3, 2, "Argent restant : %d eurofrancs", player->money);
	mvwprintw(playerData, 4, 2, "Popularite de la ligue : %d", player->fame);

	wattron(playerData, COLOR_PAIR(3));
	mvwprintw(playerData, 7, 2, "Faites ENTRER pour acheter le catcheur et F1 pour passer au menu suivant.");
	mvwprintw(playerData, 8, 2, "Vous devez avoir au moins 3 catcheurs");
	wattroff(playerData, COLOR_PAIR(3));

	refresh();
	wrefresh(title1);
	wrefresh(wmenu);
	wrefresh(playerData);

	WDisplayList(wmenusub, &pos, wList, (ElementToWrestlerName));

	element = wList->first;
	WrestlerIdentity((S_wrestler*)element->data);

	while ((ch = getch()) != 27 && (ch != KEY_F(1) || selectedList->size < 3)){

		switch (ch){

			case KEY_UP:
				pos--;
				break;

			case KEY_DOWN:
				pos++;
				break;
		}

		if (ch == '\n' && player->money >= ((S_wrestler*)element->data)->price){

			player->money = player->money - ((S_wrestler*)element->data)->price;
			AppendList(selectedList, element->data, sizeof(S_wrestler));
			PopPtnList(wList, element);

			wmove(playerData, 3, 2);
			wclrtoeol(playerData);
			mvwprintw(playerData, 3, 2, "Argent restant : %d eurofrancs", player->money);
			wborder(playerData, '|', '|', '-', '-', '+', '+', '+', '+');
			wrefresh(playerData);

		}

		element = WDisplayList(wmenusub, &pos, wList, (ElementToWrestlerName));
		WrestlerIdentity((S_wrestler*)element->data);

	}

	delwin(title1);
	delwin(wmenu);
	delwin(wmenusub);
	delwin(playerData);

	endwin();

	if (ch == 27){
		FreeWrestlerList(selectedList);
		selectedList = NULL;	
	}

	FreeWrestlerList(wList);

	return selectedList;

}
