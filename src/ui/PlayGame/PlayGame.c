/*
* @Author: klmp200
* @Date:   2016-05-29 18:52:16
* @Last Modified by:   klmp200
* @Last Modified time: 2016-06-02 21:46:33
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../getcurses.h"
#include "../../importation/importation.h"
#include "../WrestlerSelection/WrestlerSelection.h"
#include "../PlayRound/PlayRound.h"
#include "../EndCondition/EndCondition.h"
#include "../../game/game.h"
#include "../WPrintStory/WPrintStory.h"

S_game PlayGame(S_game game, char *wrestlerFolder){

	S_list *importList = NULL;
	WINDOW *storyWin;

	if (!game.mode && game.player.currentRound == 0 && game.player.currentYear == 0){
		initscr();
		storyWin = subwin(stdscr, LINES - (LINES - 3 ) / 2, COLS - 10, (LINES - 3) / 2, 5);

		WPrintStory(storyWin, 0, 12);

		delwin(storyWin);

		endwin();
	}

	while (EndCondition(game)){

		clear();

		if (game.player.currentRound == 0){

			importList = ImportWrestlers(wrestlerFolder, game.sList);
			game.wList = ConcatWrestlerList(importList, game.wList);
			game.wList = WrestlerSelection(game.wList, &game.player);

		}

		if (game.wList == NULL){
			game.save = -1;
		}

		while (!game.victory && game.player.currentRound < ROUNDS_PER_YEAR && game.save == 0){

			clear();

			if (game.wList->size > 0){

				game.save = PlayRound(game.wList, game.sList, &game.player);
				TimeSWrestler(game.wList, &game.player);

				if (game.player.money < 0){

					game.player.money = 0;

				}

			}
		}

		if (game.save == 0){

			MakeScores(&game);

		}

		if (!game.save){

			game.player.currentRound = 0;
			game.player.currentYear++;

		}

	}

	if (game.victory && !game.mode){
		initscr();
		storyWin = subwin(stdscr, LINES - (LINES - 3 ) / 2, COLS - 10, (LINES - 3) / 2, 5);
		clear();

		WPrintStory(storyWin, 13, 17);

		delwin(storyWin);

		endwin();
	}

	return game;
}