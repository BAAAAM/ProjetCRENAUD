/*
* @Author: klmp200
* @Date:   2016-05-24 23:39:20
* @Last Modified by:   klmp200
* @Last Modified time: 2016-06-01 22:48:19
*/

#include <stdio.h>
#include <stdlib.h>

#include "../getcurses.h"
#include "../../wrestler/wrestler.h"
#include "../DisplayWStatus/DisplayWStatus.h"

void WrestlerIdentity(S_wrestler *wrestler){

	WINDOW *ascii, *asciisub, *description;

	init_pair(1, COLOR_WHITE, COLOR_BLUE);

	ascii = newwin(10, 30, 1, 33);
	asciisub = subwin(ascii, 8, 28, 2, 34);
	description = subwin(stdscr, 10, 30, 16, 70);

	wborder(ascii, '|', '|', '-', '-', '+', '+', '+', '+');
	wrefresh(ascii);

	wbkgd(asciisub, COLOR_PAIR(1));

	move(3, 70);
	clrtoeol();
	printw("Nom : %s", wrestler->name);

	move(5, 70);
	clrtoeol();
	printw("Prix : %d eurofrancs", wrestler->price);

	wclear(asciisub);

	if (wrestler->ascii != NULL){

		wmove(asciisub, 0, 0);
		wprintw(asciisub, "%s", wrestler->ascii);

	} else {

		wmove(asciisub, 3, 1);
		wprintw(asciisub, "Pas de photo de profil");

	}

	wrefresh(asciisub);

	DisplayWStatus(wrestler, 14, 35);

	mvprintw(14, 70, "Informations sur le catcheur");
	wclear(description);

	wmove(description, 0, 0);

	if (wrestler->description != NULL){

		wprintw(description, "%s", wrestler->description);

	} else {

		wprintw(description, "Aucune information n'est disponible");

	}

	refresh();

	delwin(asciisub);
	delwin(ascii);
}