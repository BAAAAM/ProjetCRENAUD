/*
* @Author: klmp200
* @Date:   2016-05-29 01:08:29
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-29 01:19:46
*/

#include <stdio.h>
#include <stdlib.h>

#include "../getcurses.h"
#include "../../wrestler/wrestler.h"
#include "../../status/status.h"

void DisplayWStatus(S_wrestler *wrestler, int y, int x){
	int i;
	int j = y + 1;
	int k = j;

	for (i = 0; i < MAX_W_STATUS; i++){

		move(k + i, x);
		clrtoeol();

		if (wrestler->status[i] != NULL){

			mvprintw(j, x, "%s (%d tours)", wrestler->status[i]->name, wrestler->status[i]->rTime);
			j++;

		}

	}

	move(y, x);
	clrtoeol();

	if (j == k){

		printw("Aucun status");

	} else {

		printw("Status : ");

	}

}