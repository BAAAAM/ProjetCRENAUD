#ifndef DISPLAYWSTATUS_H_INCLUDED
#define DISPLAYWSTATUS_H_INCLUDED

void DisplayWStatus(S_wrestler *, int, int);

/*
	Display status of a wrestler
	need y and x as start positions
*/

#endif // DISPLAYWSTATUS_H_INCLUDED