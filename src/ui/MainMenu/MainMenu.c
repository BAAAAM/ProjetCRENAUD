/*
* @Author: klmp200
* @Date:   2016-05-23 09:31:06
* @Last Modified by:   klmp200
* @Last Modified time: 2016-06-02 10:01:35
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../../importation/importation.h"
#include "../getcurses.h"
#include "../WDisplayMenu/WDisplayMenu.h"

int MainMenu(){

	int ch;
	int load = 0;
	int pos = 0;
	int choice = -1;
	int firstChoice;

	char *menu[] = {
		"Aventure",
		"Arcade",
		"Credits",
		"Quitter",
	};

	char *loadSave[] = {
		"Nouvelle partie",
		"Continuer",
	};

	char *newGame[] = {
		"Nouvelle partie",
	};

	WINDOW *wmenu;
	int startx, starty, width, height;

	initscr();

	start_color();
	init_pair(1, COLOR_RED, COLOR_BLACK);
	init_pair(2, COLOR_GREEN, COLOR_BLACK);

	cbreak();
	keypad(stdscr, TRUE);
	curs_set(0);
	noecho();

	height = 5;
	width = 9;
	startx = (COLS - width) / 2;
	starty = (LINES - height) / 2;

	wmenu = subwin(stdscr, height, width, starty, startx);

	attron(COLOR_PAIR(2));
	printw("Utilisez ESC ou le boutton quitter pour quitter");
	attroff(COLOR_PAIR(2));

	attron(COLOR_PAIR(1));
	mvprintw(starty - 5, (COLS - 29) / 2, "BIENVENUE DANS C.R.E.N.A.U.D.");
	mvprintw(starty - 4, (COLS - 47) / 2, "Catch Royal Edition Nazi : An Ultimate Dictator");

	attroff(COLOR_PAIR(1));

	mvprintw(starty - 2, (COLS - 28) / 2, "Choisissez votre mode de jeu");

	refresh();
	WDisplayMenu(wmenu, pos, menu, 4);

	while (choice < 0 && (ch = getch()) != 27){

		switch (ch){

			case KEY_UP:
				pos--;
				break;

			case KEY_DOWN:
				pos++;
				break;

			case '\n':
				choice = WDisplayMenu(wmenu, pos, menu, 4);
				break;

		}

		pos = WDisplayMenu(wmenu, pos, menu, 4);
	}

	if (choice == 0 || choice == 1){

		if (choice == 0){

			firstChoice = 4;
			load = CheckFile("data/save/adventure/player.godwin");

		} else {

			firstChoice = 6;
			load = CheckFile("data/save/arcade/player.godwin");

		}

		choice = -1;

		pos = 0;

		if (load){

			WDisplayMenu(wmenu, pos, loadSave, 2);

		} else {

			WDisplayMenu(wmenu, pos, newGame, 1);

		}

		while (choice < 0 && (ch = getch()) != 27){

			switch (ch){

				case KEY_UP:
					pos--;
					break;

				case KEY_DOWN:
					pos++;
					break;

				case '\n':
					choice = (firstChoice + pos); 
					break;

				}

			if (load){

				pos = WDisplayMenu(wmenu, pos, loadSave, 2);

			} else {

				pos = WDisplayMenu(wmenu, pos, newGame, 1);

			}

		}

	}

	delwin(wmenu);
	endwin();

	return choice;

}
