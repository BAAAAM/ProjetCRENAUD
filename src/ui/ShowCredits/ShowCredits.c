/*
* @Author: klmp200
* @Date:   2016-06-02 22:03:37
* @Last Modified by:   klmp200
* @Last Modified time: 2016-06-02 22:25:11
*/

#include <stdio.h>
#include <stdlib.h>

#include "../getcurses.h"

void ShowCredits(){

	initscr();

	noecho();
	cbreak();
	start_color();

	init_pair(8, COLOR_GREEN, COLOR_BLACK);
	init_pair(9, COLOR_RED, COLOR_BLACK);

	attron(COLOR_PAIR(8));
	mvprintw((LINES - 5) / 2 - 2, (COLS - 36) / 2, "Ce programme vous est presente par :");
	attroff(COLOR_PAIR(8));
	mvprintw((LINES - 3) / 2, (COLS - 17) / 2, "Bartuccio Antoine");
	mvprintw((LINES - 2) / 2, (COLS - 14) / 2, "Amalvy Arthur");
	mvprintw(LINES / 2, (COLS - 11) / 2, "Abbe Magsen");

	attron(COLOR_PAIR(9));
	mvprintw(LINES - 1, 2, "Ce programme est disponible selon les termes de la licence MIT");
	attroff(COLOR_PAIR(9));

	refresh();

	getch();

	endwin();

}