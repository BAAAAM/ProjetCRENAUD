#ifndef WDISPLAYMENU_H_INCLUDED
#define WDISPLAYMENU_H_INCLUDED

#include "../getcurses.h"

#define DisplayMenu(n, menu, size) (WEntreMenu(stdscr, n, menu, size))

int WDisplayMenu(WINDOW *, int, char *[], int);

/*
	Display a given menu and return what have been selected
	Need a window, a position of cursor, an array of strings and his size
	
	DisplayMenu is a macro to print in stdscr
*/


#endif // WDISPLAYMENU_H_INCLUDED