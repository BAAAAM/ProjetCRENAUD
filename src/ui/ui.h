#ifndef UI_H_INCLUDED
#define UI_H_INCLUDED

#include "getcurses.h"
#include <panel.h>

#include "WDisplayMenu/WDisplayMenu.h"
#include "WDisplayList/WDisplayList.h"
#include "WGetName/WGetName.h"
#include "ProcessPosition/ProcessPosition.h"
#include "MainMenu/MainMenu.h"
#include "PlayRound/PlayRound.h"
#include "WrestlerSelection/WrestlerSelection.h"
#include "WrestlerIdentity/WrestlerIdentity.h"
#include "../wrestler/ElementToWrestlerName/ElementToWrestlerName.h"
#include "DisplayWStatus/DisplayWStatus.h"
#include "PlayGame/PlayGame.h"
#include "WPrintStory/WPrintStory.h"
#include "DisplayScoring/DisplayScoring.h"
#include "ShowCredits/ShowCredits.h"

#endif // UI_H_INCLUDED
