# Liste et Description des catcheurs présents dans le jeu ( ordre alphabétique )

## Catcheurs Normaux


* Triple Bord : Un catcheur si volumineux qu'il possèderait un nombre de bord trois fois supérieur à celui des autres catcheurs. On dit également de lui que le "triple" présent dans son nom n'est pas un hasard : il fréquenterait une certain organisation obscure au logo rappelant curieusement un oeil...

* Le Caillou : Son imposante stature et son incroyable aptitude à encaissere les coups ont fait de ce catcheur un ennemi craint et respecté.

* Le Mexicain Vert : Un catcheur atypique, ayant un style vestimentaire particulier. Il arbore constamment sa couleur fétiche, le vert, dont il est recouvert lors de ses matchs. 

* Anbois : Elle est vénère.

* Crenaud : Favori du public, on sait cependant peu sur sa véritable force... 

* Rey le très mystérieux : Catcheur très populaire, depar le mystère qu'il entretient en se cachant le visage avec un masque. Sa petite taille est compensée par son agilité phénoménale.



## Catcheurs spéciaux

Ces catcheurs ne peuvent s'obtenir qu'en réalisant certaines actions.


* CD

* Sli

* Bro : Arbore une bien étrange blouse blanche lors de ses matchs.



## Catcheurs Secrets

* ???
