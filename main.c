/*
* @Author: klmp200
* @Date:   2016-04-26 20:13:59
* @Last Modified by:   klmp200
* @Last Modified time: 2016-06-02 22:07:30
* @Last Modified time: 2016-05-24 19:53:40
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "src/importation/importation.h"
#include "src/exportation/exportation.h"
#include "src/wrestler/wrestler.h"
#include "src/status/status.h"
#include "src/lists/list.h"
#include "src/gyaml/gyaml.h"
#include "src/player/player.h"
#include "src/ui/ui.h"
#include "src/game/game.h"

int main() {

	int choice;

	S_game game = InitiateGame();

	//wList = SortWrestler(wList); Care not really work much wow

	#ifndef _WIN32

		ESCDELAY = 0;

	#else

		system("Mode Con Cols=110 Lines=40");

	#endif

	choice = MainMenu();

	clear();

	switch (choice){

		case 4:
			/* Adventure New Game */
			game.sList = ImportStatus("data/status/");
			game.mode = 0;

			GetName(&game.player);

			game = PlayGame(game, "data/wrestlers/");

			break;

		case 5:
			/* Aventure continue */
			game.mode = 0;

			game.sList = ImportStatus("data/save/adventure/status/");
			game.wList = ImportWrestlers("data/save/adventure/wrestlers/", game.sList);
			game.player = ImportPlayer("data/save/adventure/player.godwin");

			game = PlayGame(game, "data/wrestlers/");

			break;

		case 6:
			/* Arcade New Game */
			game.mode = 1;

			game.sList = ImportStatus("data/status/");
			game.wList = ImportWrestlers("data/wrestlers/", game.sList);

			GetName(&game.player);
			game = PlayGame(game, "data/wrestlers/");

			break;

		case 7:
			/* Arcade continue */
			game.mode = 1;

			game.sList = ImportStatus("data/save/arcade/status/");
			game.wList = ImportWrestlers("data/save/arcade/wrestlers/", game.sList);
			game.player = ImportPlayer("data/save/arcade/player.godwin");

			game = PlayGame(game, "data/wrestlers/");

			break;

		case 2:

			ShowCredits();

			game.save = -1;

			break;
		default:
			game.save = -1;
			break;
	}

	if (game.save == 1){

		SaveGame(game);

	}

	FreeGame(&game);

    return 0;
}
