% Projet CRENAUD
% Bartuccio Antoine \cr Amalvy Arthur \cr Abbe Magsen
% Printemps 2016

\newpage

# Présentation du projet

## Le contexte

N'avez vous jamais songé à quel point l'univers était étrange ? Et bien, vous aviez raison. Et pourtant, peu des étrangetés dont il est capables parviennent jusqu'à nos oreilles. Il se trouve qu'il existe, en parallèle à la notre, une réalité alternative. Une réalité qui, par l'étrange hasard de lois qui nous échappent, est strictement identique à la nôtre, si l'on omet un tout petit détail : dans cette réalité alternative, les Nazis sont les grands vainqueurs de la seconde guerre mondiale.


534GT, année du grand Ragondin cosmique. plus de 60 ans après la victoire allemande, le pouvoir des ligues de catchs dans le monde est inégalé. Il a depuis longtemps supplanté la piètre dictature allemande, de par son pouvoir captivant sur les masses. Chaque année, et c'est désormais une tradition qui ne saurait être reniée ou bravée, la ligue de catch la plus populaire est destinée à gouverner. C'est ainsi qu'un affrontement sans merci, où tout les coups sont permis, se déroule entre les différentes institutions pour décider de la ligue qui sera appelée à gouverner le monde l'année suivante !


Vous vous dites sans doute que tout cela n'a aucun sens. d'ailleurs, vous en êtes certain. Allons ! Et pourtant, les faits sont là. Après l'apparition d'un vortex spatio-tempo-dimensionnel dans les locaux du CERN, vos parents ont étés enlevés par des hommes provenant de cette réalité alternative, des catcheurs nazis, et sont retenus en prison dans cet univers parallèle. Vous ne me croyez pas ? Alors, comment expliquer leur disparition !? Il ne vous reste qu'une seule solution : plonger à votre tour dans le vortex, monter une ligue de catch professionnelle, devenir le Dictator de ce monde étrange, et modifier les lois pour sauver vos parents. C'est l'heure du catch Royal !

![Nommage des années du calendrier](Calendrier.png)

\newpage

## Les objectifs du projet

Comme vous l'aurez compris, le projet CRENAUD est une simulation de gestion de ligue de catch en compétition. Le principe est d'administrer votre ligue de catch de manière la plus avisée pour être la plus populaire. C'est un jeu à but humoristique dans un univers décalé suivant un calendrier d'inspiration chinoise.

Ce projet sera publié en tant que projet libre et donc, par soucis de satisfaction du plus grand nombre, tout le code sera rédigé en anglais.

\newpage


# Analyse du projet

![Déroulement du jeu](schemaDessinPortraitTableauCrenaud.png)

## Structures

```c
Variable Wrestler
	name string
	description string // will be build in the right language during the importation
	price integer
	fame integer
	strength integer
	status array of Status
```

```c
Variable Status
	name string
	mMoney integer // Paid each round for medical aids
	bFame integer
	bStrength integer
	time integer // How long this status should last
	sideEffect Status // To be applied at the end
```

## Découpage du projet

### Fonctions d'import / export depuis un fichier

* Statut
* Catcheurs
* Ascii art
* Sauvegarde
* Chargement 


### Liés aux effets 

* VérificationStatut
* ApplicationStatut
* ModificationTemps


### Liés aux catcheurs

* AjoutStatut
* RetraitStatut
* RemplacementStatut
* CompteurStatut
* AfficherStatut


### Liés à l'affichage

* AffichageMenu
* AffichageListeCatcheur
* AffichageCaractéristiqueCatcheur
* AffichageRésultatCombat
* AffichageTutoriel


### Calculs

* CalculGains (popularité + argent)
* Dépenses
* CalculStatsTemporaires (calcule les statistiques d'un catcheur en fonction de ses statuts)


### Liés au gameplay

* SelectionRoster
* FinJeu
* OrganisationCombat
* EffetsAléatoires

# Répartition des tâches

## Amalvy Arthur

* Graphiste / illustrateur
* Scénariste
* Algorithme de gestion des matchs
* Standardisation du code

## Bartuccio Antoine

* Interface
* Algorithme de gestion de fichiers
	
	* Import
	* Export
	* Sauvegarde

* Aide au scénario

## Abbe Magsen

* Gameplay et équilibrage du jeu
* Algorithmes de calcul
* Algorithmes liés aux statuts
* Algorithmes liés aux catcheurs

# Algorithmes importants

Les algorithmes les plus compliqués et important étant ceux d'import/export de fichiers et n'ayant pas encore vue en cours comment les manipuler, nous vous présentons deux simples mais utiles algorithmes.

## Algorithme de la fonction VérificationStatut 

```C
Status CheckStatus(Status current)
Types de variables
	Status
Début
	Si(current.time - 1 = 0)
		Alors
			current ← current.sideEffect
		Sinon
			current.time ← current.time - 1
	Fin Si
	Retourner current
Fin
```

## Algorithme de la fonction ApplicationStatut

```C
vide ApplyStatus(Status current, entier *strength, entier *fame)
Type de variables
	Status
Début
	*strength ← *strength + current.bStrength
	*fame ← *fame + current.bfame
Fin
```

